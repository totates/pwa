import logging from "external:@totates/logging/v1";
import { Workbox } from "workbox-window";

const logger = logging.getLogger("@totates/pwa/v1");
// logger.setLevel("WARN");


export function setLinkElement(rel, href) {
    let link = document.head.querySelector(`link[rel="${rel}"]`);
    if (!link) {
        link = document.createElement("link");
        link.rel = rel;
        document.head.appendChild(link);
    }
    link.href = href;
    return link;
}

export function setMetaElement(name, content) {
    let meta = document.head.querySelector(`meta[name="${name}"]`);
    if (!meta) {
        meta = document.createElement("meta");
        meta.name = name;
        document.head.appendChild(meta);
    }
    meta.content = content;
    return meta;
}

export async function loadManifest(href) {

    const response = await fetch(href);

    if (!response.ok)
        throw new Error(`could not load manifest for ${href}`);

    const manifest = await response.json();

    document.title = manifest.name;

    setLinkElement("manifest", href);
    setLinkElement("apple-touch-icon", manifest.icons[0].src);
    setMetaElement("description", manifest.description);
    setMetaElement("theme-color", manifest.theme_color);
}

class ProgressiveWebAppElement extends HTMLElement {

    constructor() {
        super();
        window.addEventListener(
            "languagechange", () => this.reload().catch(logger.error)
        );
        // this.useStylesheets("fontawesome:style").then(() => {
        //     const i = document.createElement("i");
        //     i.className = "fas fa-dot-circle";
        //     this.shadowRoot.appendChild(i);
        //     // fontawesome.i2svg({ node: this.shadowRoot });
        // }).catch(logger.error);
    }

    static get observedAttributes() {
        return ["sw-url", "manifest-url"];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (!newValue) {
            logger.warn("unsetting attribute is not supported"); // TODO: implement
            return;
        }
        switch (name) {
            case "sw-url": {
                if ("serviceWorker" in navigator) {
                    logger.debug("service worker at", newValue);
                    this.loadServiceWorker();
                }
                else
                    logger.info("browser does not support servive worker");
                break;
            }
            case "manifest-url": {
                logger.debug("manifest at", newValue);
                loadManifest(newValue).catch(logger.error);
                break;
            }
            default:
                logger.warn(`unexpected attribute "${name}"`);
        }
    }

    async reload() {
        await loadManifest(this.getAttribute("manifest-url"));
        this.loadServiceWorker();
    }

    loadServiceWorker() {
        if (this.workbox)
            this.workbox.update();
        else {
            // import("workbox-window").then(({ Workbox }) => {
            // }).catch(logger.error);
            this.workbox = new Workbox(this.getAttribute("sw-url"));
            this.workbox.addEventListener("activated", event => {
                // `event.isUpdate` will be true if another version of the service
                // worker was controlling the page when this version was registered.
                if (!event.isUpdate)
                    logger.debug("Service worker activated for the first time!");
                // If your service worker is configured to precache assets, those
                // assets should all be available now.

            });
            this.workbox.addEventListener("waiting", event => {
                logger.debug(
                    "A new service worker has installed, but it can't activate" +
                    "until all tabs running the current version have fully unloaded."
                );
            });
            this.workbox.addEventListener("message", event => {
                if (event.data.meta === "workbox-broadcast-update" &&
                    event.data.type === "CACHE_UPDATED") {
                    const { cacheName, updatedURL } = event.data.payload;
                    logger.debug(`A newer version of ${updatedURL} is available!`);
                    // Do something with cacheName and updatedUrl.
                    // For example, get the cached content and update
                    // the content on the page.
                    // const cache = await caches.open(cacheName);
                    // const updatedResponse = await cache.match(updatedURL);
                    // const updatedText = await updatedResponse.text();
                }
            });
            this.workbox.addEventListener("activated", event => {
                // Get the current page URL + all resources the page loaded.
                const urlsToCache = [
                    // location.href,
                    // ...performance.getEntriesByType("resource").map((r) => r.name),
                ];
                // Send that list of URLs to your router in the service worker.
                // this.messageSW({ type: "CACHE_URLS", payload: {urlsToCache}, });
            });
            this.workbox.addEventListener("installed", event => {
                logger.debug("installed");
            });
            this.workbox.addEventListener("externalinstalled", event => {
                logger.debug("externalinstalled");
            });
            this.workbox.addEventListener("externalwaiting", event => {
                logger.debug("externalwaiting");
            });
            this.workbox.addEventListener("externalactivated", event => {
                logger.debug("externalactivated");
            });
            this.workbox.register();
        }
    }
}

window.customElements.define("progressive-web-app", ProgressiveWebAppElement);
// addCustomElement("progressive-web-app", ProgressiveWebAppElement.observedAttributes);
